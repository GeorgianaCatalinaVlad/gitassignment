package ro.ase.csie.cts.g1085.tema2;

public interface IMetodeVanzare {

	public int ScadereStoc() throws Exception;
	public double AcordareDiscount();
}
