package ro.ase.csie.cts.g1085.tema2;

public class Vanzari implements IMetodeVanzare {

	private int nrBon;
	private String denumireMedicament;
	private double pretMedicament;
	private int cantitateSolicitata;
	private int stocMedicament;

	public Vanzari(int nrBon, String denumireMedicament, double pretMedicament, int cantitateSolicitata,
			int stocMedicament) {
		super();
		this.nrBon = nrBon;
		this.denumireMedicament = denumireMedicament;
		this.pretMedicament = pretMedicament;
		this.cantitateSolicitata = cantitateSolicitata;
		this.stocMedicament = stocMedicament;
	}

	@Override
	public int ScadereStoc() throws Exception {
		if (this.cantitateSolicitata < this.stocMedicament) {
			this.stocMedicament = this.stocMedicament - this.cantitateSolicitata;
		} else {
			throw new Exception(
					"Cantitatea ceruta depaseste limita stocului disponibil! Incercati o valoare mai mica.");
		}
		return this.stocMedicament;
	}

	@Override
	public double AcordareDiscount() {
		double discount = 3.00;
		double limitaPretAcordareDiscount = 20.00;
		double pretFinal = this.pretMedicament > limitaPretAcordareDiscount ? this.pretMedicament - discount
				: this.pretMedicament;
		return pretFinal;
	}

}
